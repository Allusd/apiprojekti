

-- -----------------------------------------------------
-- Schema projekti_db
-- -----------------------------------------------------

CREATE SCHEMA IF NOT EXISTS `projekti_db` DEFAULT CHARACTER SET utf8 ;
USE `projekti_db` ;

-- -----------------------------------------------------
-- Table `example_db`.`Location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projekti_db`.`Tuotteet` (
  `Tuote_id` INT NOT NULL AUTO_INCREMENT,
  `Tuote_nimi` VARCHAR(45) NULL,
  `Kuvaus` VARCHAR(255) NULL,
  `LKM` INT DEFAULT 1,
  `Toimipiste_id` INT,
  `Kategoria_id` INT NULL,
  PRIMARY KEY (`Tuote_id`),
  INDEX `LKM` (`LKM` ASC))
ENGINE = InnoDB;
 
-- -----------------------------------------------------
-- Table `example_db`.`Event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projekti_db`.`Toimipisteet` (
  `Toimipiste_id` INT NOT NULL AUTO_INCREMENT,
  `Toimipiste_nimi` VARCHAR(255) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `example_db`.`Event_date`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projekti_db`.`Tuotekategoriat` (
  `Kategoria_id` INT NOT NULL AUTO_INCREMENT,
  `Kategoria_nimi` VARCHAR(255) NULL,
  PRIMARY KEY (`Kategoria_id`))
ENGINE = InnoDB;


Insert into Tuotekategoriat (Kategoria_nimi) values('Puhelin Tarvikkeet');
Insert into Tuotekategoriat (Kategoria_nimi) values('Tietokone Tarvikkeet');
Insert into Tuotekategoriat (Kategoria_nimi) values('Kannettavat tietokoneet');
Insert into Tuotekategoriat (Kategoria_nimi) values('Pöytä tietokoneet');
Insert into Tuotekategoriat (Kategoria_nimi) values('Tietokone Komponentit');


Insert into Toimipisteet (Toimipiste_nimi) values('Helsinki');
Insert into Toimipisteet (Toimipiste_nimi) values('Tampere');
Insert into Toimipisteet (Toimipiste_nimi) values('Jyväskylä');
Insert into Toimipisteet (Toimipiste_nimi) values('Oulu');
Insert into Toimipisteet (Toimipiste_nimi) values('Rovaniemi');

Insert into Tuotteet (Tuote_nimi, Kuvaus, Toimipiste_id, Kategoria_id) values('Puhelin', 'puhelin', 1, 1);
Insert into Tuotteet (Tuote_nimi, Kuvaus, Toimipiste_id, Kategoria_id) values('Kannettava tietokone', 'laptop', 2, 4);
Insert into Tuotteet (Tuote_nimi, Kuvaus, Toimipiste_id, Kategoria_id) values('Kannettava tietokone2', 'laptop', 3, 4);
Insert into Tuotteet (Tuote_nimi, Kuvaus, Toimipiste_id, Kategoria_id) values('Kannettava tietokone3', 'laptop', 4, 4);
Insert into Tuotteet (Tuote_nimi, Kuvaus, Toimipiste_id, Kategoria_id) values('MACPC', 'pöytätietokone', 1, 5);
Insert into Tuotteet (Tuote_nimi, Kuvaus, Toimipiste_id, Kategoria_id) values('Virtajohto', 'oheistuote-tietokoneet', 1, 1);

