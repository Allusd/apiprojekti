/* eslint-disable semi */
// load our app server using express

const express = require("express");
const morgan = require("morgan");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({ type: 'application/*+json' }));
app.use(express.static("./public"));
const router = express.Router();
app.use(router);

app.use(morgan("dev"));

app.listen(3003, ()=>{
    console.log("Server is running");
});

// tuotteen lisäys
// response ottaa LisaaFormin tiedot sisäänsä ja lähettää tietokantaan
// ja palataan indexiin
app.post("/lisaa_tuote", (req, res)=> {
    var response = {
        tuoteNimi: req.body.TuoteNimi,
        tuoteKuvaus: req.body.kuvaus,
        lkm: req.body.lkm,
        paikka: req.body.paikka,
        kategoria: req.body.kategoria
    };
        var Nimi = response.tuoteNimi;
        var kuvaus = response.tuoteKuvaus;
        var LKM = response.lkm;
        var paikka = response.paikka;
        var kategoria = response.kategoria;
        console.log(response);
        const conn = mysql.createConnection({
            host: "localhost",
            //port:3306,
            user: "root",
            password: "Haallu12",
            database: "projekti_db"
        });
            conn.query("INSERT INTO tuotteet (Tuote_nimi, Kuvaus, LKM, Toimipiste_id, Kategoria_id) VALUES(?,?,?,?,?)",[Nimi, kuvaus, LKM, paikka, kategoria],(err, row, fields)=> {
                if(err){
                    console.log("Error Querying");
                    res.sendStatus(500);
                }
                console.log("Success");
                res.sendfile("./public/Index.html");
            });

    });


// otetaan HaeFormin tiedot sisään ja haetaan nimellä tietokannasta
app.post("/hae_tuote", (req, res)=>{
    var response = {
        Nimi: req.body.HaeTuoteNimella
    };
    var nimi = response.Nimi;
    console.log(nimi);
    const conn = mysql.createConnection({
        host: "localhost",
        //port:3306,
        user: "root",
        password: "Haallu12",
        database: "projekti_db"
    });
    console.log(response);
    conn.query("SELECT * FROM tuotteet WHERE Tuote_nimi=?",[nimi],(err, row, fields)=> {
        if(err){
            console.log("Error Querying");
            res.sendStatus(500);
        }
        console.log(row);
        res.send(row);
    });

});

// api get kutsu ylemmälle postille
app.get("/hae/:nimi", (req, res)=>{
    console.log("haetaan nimellä " + req.params.nimi);
    var nimi = req.params.nimi;
    const conn = mysql.createConnection({
        host: "localhost",
        //port:3306,
        user: "root",
        password: "Haallu12",
        database: "projekti_db"
    });
    console.log(nimi)
    conn.query("SELECT * FROM tuotteet WHERE Tuote_nimi=?",[nimi],(err, row, fields)=> {
        if(err){
            console.log("Error Querying");
            res.sendStatus(500);
        }
        console.log(row);
        res.send(row);

    });

});

// get kutsu kaikille tuotteille
app.get("/tuotteet", (req, res)=>{
    const conn = mysql.createConnection({
        host: "localhost",
        //port:3306,
        user: "root",
        password: "Haallu12",
        database: "projekti_db"
    });
    conn.query("SELECT * FROM tuotteet ",(err, row, fields)=> {
        if(err){
            console.log("Error Querying");
            res.sendStatus(500);
        }
        console.log(row);
        res.send(row)
    });

});

// get kutsu kaikille toimipisteille
app.get("/toimipisteet", (req, res)=>{
    const conn = mysql.createConnection({
        host:"localhost",
        //port:3306,
        user:"root",
        password:"Haallu12",
        database:"projekti_db"
    });

    conn.query("SELECT * FROM toimipisteet",(err, row, fields)=>{
        if(err){
            console.log("Error Querying");
            res.sendStatus(500);
        }
        console.log("Success");
        res.json(row);
    });
});

// get kutsu kaikille tuotekategorioille
app.get("/tuotekategoriat", (req, res)=>{
    const conn = mysql.createConnection({
        host:"localhost",
        //port:3306,
        user:"root",
        password:"Haallu12",
        database:"projekti_db"
    });

    conn.query("SELECT * FROM tuotekategoriat",(err, row, fields)=>{
        if(err){
            console.log("Error Querying");
            res.sendStatus(500);
        }
        console.log("Success");
        res.json(row);
    });
});

// haetaan kaikkien kolmen pödyän data
// ja liitetään ne yhteen toimipaikka_id:n ja toimipiste_id:n avulla yhteen käyttäen mySQL JOIN ON
// "JOIN tuotekategoriat ON tuotteet.Kategoria_id = tuotekategoriat.Kategoria_Id JOIN toimipisteet ON tuotteet.Toimipiste_id = toimipisteet.Toimipiste_Id"
app.get("/Kaikki_data", (req, res)=>{
    const conn = mysql.createConnection({
        host:"localhost",
        //port:3306,
        user:"root",
        password:"Haallu12",
        database:"projekti_db"
    });

    conn.query("SELECT tuotteet.Tuote_id, tuotteet.Tuote_nimi, tuotteet.Kuvaus, tuotteet.LKM, tuotteet.Toimipiste_id, tuotteet.Kategoria_id, tuotekategoriat.Kategoria_nimi, toimipisteet.Toimipiste_nimi" +
        " FROM tuotteet JOIN tuotekategoriat ON tuotteet.Kategoria_id = tuotekategoriat.Kategoria_Id JOIN toimipisteet ON tuotteet.Toimipiste_id = toimipisteet.Toimipiste_Id",(err, row, fields)=>{
        if(err){
            console.log("Error Querying");
            res.sendStatus(500);
        }
        console.log("Success");
        res.json(row);
    });
});

// kutsu tuotteen haulle id:n avulla
// ei käytetty lopullisessa
// ideana oli haku nimellä tai id:llä
app.get("/tuotteet/:id", (req, res)=>{
    const conn = mysql.createConnection({
        host:"localhost",
        //port:3306,
        user:"root",
        password:"Haallu12",
        database:"projekti_db"
    });
    const idNumber = req.params.id;
    const sql = "SELECT * FROM tuotteet WHERE Tuote_id = ?";
    conn.query(sql,[idNumber],(err, row, fields)=>{
        if(err){
            console.log("Error Querying");
            res.sendStatus(500);
        }
        console.log("Success");
        res.json(row);
    });
});

// DeleteForm.html käytetty post
app.post("/delete", (req, res)=>{
    const conn = mysql.createConnection({
        host:"localhost",
        //port:3306,
        user:"root",
        password:"Haallu12",
        database:"projekti_db"
    });
    var response = {
        Nimi: req.body.poistoNimi
    };
    var nimi = response.Nimi;
    const sql = mysql.format("DELETE FROM tuotteet WHERE Tuote_nimi=?",[nimi]);
    conn.query(sql,function(err, row, fields){
        if(err){
            console.log("Error Querying");
            res.sendStatus(500);
        }
        res.sendfile("./public/Index.html");

    });
});

// EditForm.html käytetty post
app.post("/edit", (req, res)=> {
    const conn = mysql.createConnection({
        host: "localhost",
        //port:3306,
        user: "root",
        password: "Haallu12",
        database: "projekti_db"
    });
    var response = {
        Nimi: req.body.TuotteenNimi,
        lukumaara: req.body.numero
    };
    var nimi = response.Nimi;
    var lkm = response.lukumaara;
    console.log(nimi);

    const sql2 = mysql.format("UPDATE tuotteet SET LKM=? WHERE Tuote_nimi=?", [lkm, nimi]);
    conn.query(sql2, function (err, row, fields) {
        if(err){
            console.log("Error Querying");
            res.sendStatus(500);
        }
        res.sendfile("./public/Index.html");
    });
});



