APIPROJEKTI NodeJS
-----

Info
-

Creators: Aleksi Leppimaa, Simo Mattila


Dependencies:
-
    "body-parser": "^1.18.3",
    "connect": "^3.6.6",
    "express": "^4.16.3",
    "morgan": "^1.9.1",
    "mysql": "^2.16.0",
    "nodemon": "^1.18.4",
    "react": "^16.5.2",
    "routes": "^2.1.0"
    
DATABASE
-
Example database provided: ProjektiDatabaseV1.sql

API Calls
-
  http://localhost:3003/tuotteet // palauttaa pöydeän tuotteet
  http://localhost:3003/tuotteet/:id // palauttaa tuotteen id:n perusteella
  http://localhost:3003/tuotekategoriat // palauttaa pöydeän tuotekategoriat
  http://localhost:3003/toimipisteet    // palauttaa pöydeän toimipisteet
  http://localhost:3003/Kaikki_data     // palauttaa kaikkien 3 pöydän datan linkattuina id:n perusteella

--

EXAMPLE CALLS
--
http://localhost:3003/tuotteet/2

Response:

[
{
"Tuote_id": 2,
"Tuote_nimi": "Kannettava tietokone",
"Kuvaus": "laptop",
"LKM": 1,
"Toimipiste_id": 2,
"Kategoria_id": 4
}
]


http://localhost:3003/Kaikki_data

[
{
"Tuote_id": 2,
"Tuote_nimi": "Kannettava tietokone",
"Kuvaus": "laptop",
"LKM": 1,
"Toimipiste_id": 2,
"Kategoria_id": 4,
"Kategoria_nimi": "Pöytä tietokoneet",
"Toimipiste_nimi": "Tampere"
},
{
"Tuote_id": 3,
"Tuote_nimi": "Kannettava tietokone2",
"Kuvaus": "laptop",
"LKM": 1,
"Toimipiste_id": 3,
"Kategoria_id": 4,
"Kategoria_nimi": "Pöytä tietokoneet",
"Toimipiste_nimi": "Jyväskylä"
},
